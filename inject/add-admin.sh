#!/bin/bash

user=$1
pass=$2

if [[ -z $user || -z $pass ]]; then
    echo "Usage: $(basename $0) user pass" >&2
    exit 1
fi

# create
useradd \
    --shell /bin/bash \
    --home-dir /home/$user --create-home \
    --user-group --groups student \
    --password "$(openssl passwd -stdin <<< $pass)" \
    $user

## allow to install packages with sudo and to manage users (create/backup/restore)
cat <<EOF  > /etc/sudoers.d/$user
$user ALL = /usr/local/bin/apt
$user ALL = /usr/local/bin/users.sh
EOF
chmod 0440 /etc/sudoers.d/$user
echo "alias sudo='sudo -h 127.0.0.1'" >> /home/$user/.bash_aliases

# set color prompt
sed -i /home/$user/.bashrc -e '/^#force_color_prompt=/c force_color_prompt=yes'
