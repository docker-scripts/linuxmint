APP="linuxmint"

#FLAVOR='cinnamon'
FLAVOR='mate'
#FLAVOR='xfce'

### Docker settings.
IMAGE="dockerscripts/linuxmint:$FLAVOR"
#IMAGE="dockerscripts/linuxmint:$FLAVOR-minimal"
#IMAGE="dockerscripts/linuxmint:$FLAVOR-edu"

### Forwarded ports
#X2GO_PORT="2202"
#PORTS="$X2GO_PORT:22"

### Epoptes admins. Uncomment to enable.
#EPOPTES_USERS="user1 user2"

### Admin account. Uncomment to enable.
#ADMIN_USER="admin"
#ADMIN_PASS="pass"

### Guest account. Uncomment to enable.
### This account will be used as a template for guest accounts.
#GUEST_USER="guest"
#GUEST_PASS="pass"
