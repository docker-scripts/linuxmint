cmd_guest-accounts_help() {
    cat <<_EOF
    guest-accounts (gen | generate) <nr> [<basename>]
        Output to stdout <nr> lines with 'username:password'.
        If <basename> is not given, the default value will be 'guest'
        and the usernames will be 'guest1', 'guest2', etc.

    guest-accounts create-lm <guests.txt>
        Create the guest accounts listed on the given file <guests.txt>.
        Any existing guest accounts will be deleted first.
        Each line on the file <guests.txt> looks like this:
            username:password
        If the passord is missing, the username will be used as password.

    guest-accounts create-guac <guests.txt> [<guac-connection>]
        Same as 'create-lm' but creates guest accounts on guacamole.

    guest-accounts (del | delete) [<guac-connection>]
        Delete any existings guest accounts.

_EOF
}

cmd_guest-accounts() {
    local cmd=$1; shift
    case $cmd in
        gen|generate)
            _generate_guest_accounts "$@"
            ;;
        create-lm)
            _create_lm_guest_accounts "$@"
            ;;
        create-guac)
            _create_guac_guest_accounts "$@"
            ;;
        del|delete)
            _delete_guest_accounts
            ;;
        *)
            fail "Usage:\n$(cmd_guest-accounts_help)"
            ;;
    esac
}

_generate_guest_accounts() {
    local nr=$1
    local base=${2:-guest}

    # check if the first argument is a number
    case $nr in
        *[!0-9]*)
            fail "Error: The number of guest accounts must be an integer."
            ;;
        '')
            echo -e "Usage:\n$(cmd_guest-accounts_help)"
            return 0
            ;;
    esac

    # print guest accounts
    for i in $(seq $nr); do
        echo "${base}${i}:$(pwgen)"
    done
}

_create_lm_guest_accounts() {
    local file=$1
    [[ -f $file ]] || fail "Error: Cannot find '$file'"

    # create guest accounts on linuxmint
    ds users remove-guests
    ds users create-guests $file
}

_create_guac_guest_accounts() {
    local file=$1
    [[ -f $file ]] || fail "Error: Cannot find '$file'"

    local conn=$2
    if [[ -z $conn ]]; then
        conn="$CONTAINER:rdp:guest"
        ds guac guest remove $conn
        ds guac conn rm $conn
        ds guac conn add $CONTAINER rdp --guest
    else
        echo $conn | grep -qE ':guest(:|$)' || fail "'$conn' is not a guest connection"
        ds guac guest remove $conn
    fi

    # create guest accounts on guacamole
    local baseurl=$(ds guac url)
    cat $file | while IFS=: read username password; do
        [[ -z $password ]] && password=$username
        ds guac guest add $conn $username $password
        echo "$baseurl/?username=$username&password=$password"
    done
}

_delete_guest_accounts() {
    ds users remove-guests

    local conn=$1
    [[ -z $conn ]] && conn="$CONTAINER:rdp:guest"
    echo $conn | grep -qE ':guest(:|$)' || fail "'$conn' is not a guest connection"

    ds guac guest remove $conn
    ds guac conn rm $conn
}
