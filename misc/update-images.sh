#!/bin/bash -x

ds pull linuxmint
rm -rf /var/ds/build-lm-images/
ds init linuxmint @build-lm-images
cd /var/ds/build-lm-images/

# get the latest version of ubuntu image
docker pull ubuntu:24.04

for flavor in cinnamon mate xfce ; do
    # minimal
    sed -i settings.sh \
	-e '/FLAVOR=/d' \
	-e '/IMAGE=/d'
    sed -i settings.sh \
	-e "/APP=/ a FLAVOR='$flavor'" \
	-e '/### Docker settings./ a IMAGE="dockerscripts/linuxmint:$FLAVOR-minimal"'
    rm -f packages
    ds build
    docker push dockerscripts/linuxmint:$flavor-minimal

    # normal size
    sed -i settings.sh \
	-e '/IMAGE=/ c IMAGE="dockerscripts/linuxmint:$FLAVOR"'
    cp /opt/docker-scripts/linuxmint/packages.sample packages
    sed -i packages \
	-e '/### edu packages/,$ d'
    ds build
    docker push dockerscripts/linuxmint:$flavor

    # edu
    sed -i settings.sh \
	-e '/IMAGE=/ c IMAGE="dockerscripts/linuxmint:$FLAVOR-edu"'
    cp /opt/docker-scripts/linuxmint/packages.sample packages
    ds build
    docker push dockerscripts/linuxmint:$flavor-edu

    docker container prune --force
    docker rmi dockerscripts/linuxmint:$flavor-edu
    docker rmi dockerscripts/linuxmint:$flavor
done

# clean up
cd ..
rm -rf /var/ds/build-lm-images/
docker container prune --force
docker image prune --force

########## build LMDE images ###########

ds pull linuxmint lmde
ds init linuxmint-lmde @build-lm-images
cd /var/ds/build-lm-images/

# get the latest version of debian image
docker pull debian:bullseye

# build the minimal image
sed -i settings.sh \
    -e '/IMAGE=/d'
sed -i settings.sh \
    -e '/### Docker settings./ a IMAGE="dockerscripts/linuxmint:lmde-minimal"'
ds build
docker push dockerscripts/linuxmint:lmde-minimal

# normal size
sed -i settings.sh \
    -e '/IMAGE=/ c IMAGE="dockerscripts/linuxmint:lmde"'
cp /opt/docker-scripts/linuxmint-lmde/packages.sample packages
sed -i packages \
    -e '/### edu packages/,$ d'
ds build
docker push dockerscripts/linuxmint:lmde

# edu
sed -i settings.sh \
    -e '/IMAGE=/ c IMAGE="dockerscripts/linuxmint:lmde-edu"'
cp /opt/docker-scripts/linuxmint-lmde/packages.sample packages
ds build
docker push dockerscripts/linuxmint:lmde-edu

# clean up
cd ..
rm -rf build-lm-images/
docker container prune --force
docker image prune --force
rm -rf /opt/docker-scripts/linuxmint-lmde
